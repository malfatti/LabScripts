/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-12-20
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

  A = pulse on Pins[0]
  B = pulse on Pins[1]
  C = pulse on Pins[2]
  D = pulse on Pins[3]
  E = pulse on Pins[4]
  F = pulse on Pins[5]
  G = pulse on Pins[6]
  P = pulse on Pins[7]
*/

const int inPin = A0;
const int PinNo = 8;
const int Pins[PinNo] = {39, 41, 43, 45, 47, 49, 51, 53};
const int Delay = 10;

void setup() {
  Serial.begin(115200);

//  // Set free running mode on ADC7 (pin A0)
//  ADC->ADC_MR |= 0x80;
//  ADC->ADC_CR = 2;
//  ADC->ADC_CHER = 0x80;

  pinMode(inPin, INPUT);
  for (int Pin = 0; Pin < PinNo; Pin++) {
    pinMode(Pins[Pin], OUTPUT);
    digitalWrite(Pins[Pin], LOW);
  }

  char ch = 0;
  int inPinV = 0;
}

void loop() {
  char ch = 0;
  int inPinV = 0;

  while (ch == 0) {
    ch = Serial.read();
//    while ((ADC->ADC_ISR & 0x80) == 0); // wait for conversion
//    inPinV = ADC->ADC_CDR[7];
//    
//    if (inPinV > 150) {
//      ch = -1;
//    }
  }

  if (ch == 'A') {
    digitalWrite(Pins[0], HIGH);
    delay(Delay);
    digitalWrite(Pins[0], LOW);
  }

  if (ch == 'a') {
    digitalWrite(Pins[0], HIGH);
    while (ch != 'z') {
      ch = Serial.read();
    }
    digitalWrite(Pins[0], LOW);
  }


  if (ch == 'B') {
    digitalWrite(Pins[1], HIGH);
    delay(Delay);
    digitalWrite(Pins[1], LOW);
  }

  if (ch == 'b') {
    digitalWrite(Pins[1], HIGH);
    while (ch != 'y') {
      ch = Serial.read();
    }
    digitalWrite(Pins[1], LOW);
  }

  if (ch == 'C') {
    digitalWrite(Pins[2], HIGH);
    delay(Delay);
    digitalWrite(Pins[2], LOW);
  }

  if (ch == 'c') {
    digitalWrite(Pins[2], HIGH);
    while (ch != 'x') {
      ch = Serial.read();
    }
    digitalWrite(Pins[2], LOW);
  }

  if (ch == 'D') {
    digitalWrite(Pins[3], HIGH);
    delay(Delay);
    digitalWrite(Pins[3], LOW);
  }

  if (ch == 'd') {
    digitalWrite(Pins[3], HIGH);
    while (ch != 'w') {
      ch = Serial.read();
    }
    digitalWrite(Pins[3], LOW);
  }

  if (ch == 'E') {
    digitalWrite(Pins[4], HIGH);
    delay(Delay);
    digitalWrite(Pins[4], LOW);
  }

  if (ch == 'e') {
    digitalWrite(Pins[4], HIGH);
    while (ch != 'v') {
      ch = Serial.read();
    }
    digitalWrite(Pins[4], LOW);
  }

  if (ch == 'F') {
    digitalWrite(Pins[5], HIGH);
    delay(Delay);
    digitalWrite(Pins[5], LOW);
  }

  if (ch == 'f') {
    digitalWrite(Pins[5], HIGH);
    while (ch != 'u') {
      ch = Serial.read();
    }
    digitalWrite(Pins[5], LOW);
  }

  if (ch == 'G') {
    digitalWrite(Pins[6], HIGH);
    delay(Delay);
    digitalWrite(Pins[6], LOW);
  }

  if (ch == 'g') {
    digitalWrite(Pins[6], HIGH);
    while (ch != 't') {
      ch = Serial.read();
    }
    digitalWrite(Pins[6], LOW);
  }

  if (ch == 'P') {
    digitalWrite(Pins[7], HIGH);
    delay(Delay);
    digitalWrite(Pins[7], LOW);
  }

  if (ch == 'T') {
    delay(Delay);

    while (true) {
    //for (int Trial = 0; Trial < 10; Trial++) {
      for (int Pulse = 0; Pulse < 10; Pulse++) {
        digitalWrite(Pins[7], HIGH); delay(15); 
        digitalWrite(Pins[7], LOW); delay(85);
      }
      
      digitalWrite(Pins[7], HIGH); delay(5000); 
      digitalWrite(Pins[7], LOW);
      delay(15000);
      //if (Trial < 9) { delay(15000); }
    }
  }

//  if (inPinV < 200) {
//    digitalWrite(Pins[0], LOW);
//    digitalWrite(Pins[1], LOW);
//  }
//
//  if (inPinV >= 215 && inPinV < 275) {
//    digitalWrite(Pins[0], LOW);
//    digitalWrite(Pins[1], HIGH);
//  }
//
//  if (inPinV >= 550 && inPinV < 570) {
//    digitalWrite(Pins[0], HIGH);
//    digitalWrite(Pins[1], LOW);
//  }
//
//  if (inPinV >= 800) {
//    digitalWrite(Pins[0], HIGH);
//    digitalWrite(Pins[1], HIGH);
//  }
//
}
