PinHeight = 100; HeadHeight = 25;
PinWidth = 10; HeadWidth = 15;
PinScrew = 6; HeadScrew = 4; ScrewRes = 100;

Thick = 4;
HeadSides = (HeadHeight-HeadWidth)/2;

module PinBase(){
    //translate([-PinHeight/2, 0, 0]) rotate([45,0,0]) cube([PinHeight, PinWidth, PinWidth]);
    polyhedron(points = [
        [PinHeight/2, -PinWidth/2, Thick], 
        [-PinHeight/2, -PinWidth/2, Thick], 
        [-PinHeight/2, 0, 0], 
        [PinHeight/2, 0, 0], 
        [PinHeight/2, PinWidth/2, Thick],
        [-PinHeight/2, PinWidth/2, Thick],
    ], faces = [
        [0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]
    ]);
}

module HeadBase(){
    translate([-HeadHeight/2, -HeadWidth/2, 0]) cube([HeadHeight, HeadWidth, Thick]);
}

module PinHoles(){
    translate([PinHeight/2-PinScrew*2, 0, 0]) cylinder(h=Thick, r=PinScrew/2, $fn=ScrewRes);
}

module HeadHoles(){
    translate([HeadHeight/4, 0, 0]) cylinder(h=Thick, r=HeadScrew/2, $fn=ScrewRes);
}

module Pin(){difference(){PinBase(); PinHoles();}}
module Head(){difference(){HeadBase(); HeadHoles();}}

module HeadTop(){
    union(){
        translate([0, HeadWidth+PinWidth, 0]) Head();
        translate([-HeadHeight/6, HeadWidth/2+PinWidth-HeadSides, 0]) cube([HeadHeight*2/3, HeadSides, Thick*2]);
        translate([-HeadHeight/6, HeadWidth+PinWidth+HeadSides*1.5, 0]) cube([HeadHeight*2/3, HeadSides, Thick*2]);
    }
}


module Holder(){
    union(){
        Pin(); translate([-PinHeight/2-HeadHeight/2, 0, 0]) Head();
    }
}



//HeadHoles();
//Head();
//Pin();
Holder(); HeadTop();
