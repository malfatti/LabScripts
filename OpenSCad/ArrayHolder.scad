BaseHeight = 40; BaseWidth = 40; BaseThick = 14;
TeethDepth = 10;
ScrewD = 6; ScrewRes = 100;

ScrewZOffset = BaseThick - TeethDepth/2;

module Base(){
    union(){
        cube([BaseHeight, BaseWidth, BaseThick]);
    }
}

module Holes(){
    translate([TeethDepth/2, TeethDepth/2, BaseThick-TeethDepth]) cube([TeethDepth, BaseHeight-TeethDepth, TeethDepth]);
    translate([(BaseWidth-TeethDepth/2)-TeethDepth, TeethDepth/2, BaseThick-TeethDepth]) cube([TeethDepth, BaseHeight-TeethDepth, TeethDepth]);
    
    translate([TeethDepth, BaseThick-TeethDepth/2, BaseThick/2]) rotate([90, 0, 0]) cylinder(h=TeethDepth/2, r=ScrewD/2, $fn=ScrewRes);
}

difference(){Base(); Holes();}