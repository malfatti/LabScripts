#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20180420
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import numpy as np
import scipy.io as sio
from glob import glob

from sciscripts.IO import Hdf5
from sciscripts.IO.Txt import DictFlat

Files = glob('/home/cerebro/Martina/**/Anim*.hdf5', recursive=True)
OutPath = '/home/cerebro/Martina/Mat'

for File in Files:
    FileName = OutPath + '/' + File.split('/')[-1][:-4]+'mat'
    if FileName in glob(OutPath+'/*'):
        print('Skipping', File, '...')
        continue

    print('Loading', File, '...')
    Data = Hdf5.DataLoad('/', File)[0]
    Data = DictFlat(Data)

    Keys = {K: K.replace('-', '_') for K in Data.keys()}
    for Old,New in Keys.items(): Data[New] = Data.pop(Old)

    KeysToDel = []; KeysToAdd = {}
    for K, V in Data.items():
        if 'Spks' in K:
            SpksKey = '_'.join(K.split('_')[:-1])
            SpkNo = int(K.split('_')[-1])
            SpksNo = len([k for k in Data.keys() if SpksKey in k])

            if SpksKey not in KeysToAdd: KeysToAdd[SpksKey] = np.zeros((V.shape[0], SpksNo))
            KeysToAdd[SpksKey][:,SpkNo] = V

            KeysToDel.append(K)

    for K in KeysToDel: del(Data[K])
    Data = {**Data, **KeysToAdd}

    sio.savemat(FileName, Data)
    print('Written to', FileName, '.')
