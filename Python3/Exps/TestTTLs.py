# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2015
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import time

from sciscripts.IO import Arduino

ArduinoObj = Arduino.CreateObj(115200)

#%% Test TTL connections
while True:
    ArduinoObj.write(b'A')
    time.sleep(0.08)
    ArduinoObj.write(b'B')
    time.sleep(0.08)
    ArduinoObj.write(b'C')
    time.sleep(0.08)
    ArduinoObj.write(b'D')
    time.sleep(0.08)
    ArduinoObj.write(b'E')
    time.sleep(0.08)
    ArduinoObj.write(b'F')
    time.sleep(0.08)
    ArduinoObj.write(b'G')
    time.sleep(0.08)
    ArduinoObj.write(b'P')
    time.sleep(3)

#%% Test Sound-Arduino TTLs
Stimulation['Stim'].start()

for _ in range(100):
    Stimulation['Stim'].write(np.ascontiguousarray(Stimulation['Sound'][:,:,0,0]))
    time.sleep(0.09)

for _ in range(100):
    Stimulation['Stim'].write(np.ascontiguousarray(Stimulation['Laser'][:,:,0,0]))
    Stimulation.write(Laser)
time.sleep(3)

for _ in range(100):
    Stimulation.write(SoundLaser[0][0])
time.sleep(3)


#%% Test Intan RHA 120-145 40-70 180<
print('Testing serial TTLs...')
for _ in range(10):
    ArduinoObj.write(b'A')
    time.sleep(0.08)
    ArduinoObj.write(b'B')
    time.sleep(0.08)
    ArduinoObj.write(b'C')
    time.sleep(0.08)
    ArduinoObj.write(b'D')
    time.sleep(0.08)
    ArduinoObj.write(b'E')
    time.sleep(0.08)
    ArduinoObj.write(b'P')
    time.sleep(3)

#%% Test Sound-Arduino TTLs
print('Testing sound TTLs...')
for _ in range(100):
    Stimulation.write(Sound[0][0])
time.sleep(3)

for _ in range(100):
    Stimulation.write(Laser)
time.sleep(3)

for _ in range(100):
    Stimulation.write(SoundLaser[0][0])
time.sleep(3)

#%% Test OpenEphys RecControl
i = 0
# ArduinoObj.write(b'd')
while True:
    i += 1; print(i)
    ArduinoObj.write(b'd')
    time.sleep(3)
    ArduinoObj.write(b'w')
    time.sleep(2)
# ArduinoObj.write(b'w')

