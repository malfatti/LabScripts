from imutils.video.pivideostream import PiVideoStream
from imutils.video import FPS
from picamera.array import PiRGBArray
from picamera import PiCamera
from threading import Condition, Thread

import numpy as np
import time
import cv2
import RPi.GPIO as GPIO

# class PiVideoStream:
    # """
    # Stolen from Adrian Rosebrock
    # @ https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/
    # """
	# def __init__(self, resolution=(640, 480), framerate=32):
		# # initialize the camera and stream
		# self.camera = PiCamera()
		# self.camera.resolution = resolution
		# self.camera.framerate = framerate
		# self.rawCapture = PiRGBArray(self.camera, size=resolution)
		# self.stream = self.camera.capture_continuous(self.rawCapture,
			# format="bgr", use_video_port=True)
		# # initialize the frame and the variable used to indicate
		# # if the thread should be stopped
		# self.frame = None
		# self.stopped = False
#
    # def start(self):
		# # start the thread to read frames from the video stream
		# Thread(target=self.update, args=()).start()
		# return self
#
	# def update(self):
		# # keep looping infinitely until the thread is stopped
		# for f in self.stream:
			# # grab the frame from the stream and clear the stream in
			# # preparation for the next frame
			# self.frame = f.array
			# self.rawCapture.truncate(0)
			# # if the thread indicator variable is set, stop the thread
			# # and resource camera resources
			# if self.stopped:
				# self.stream.close()
				# self.rawCapture.close()
				# self.camera.close()
				# return
#
    # def read(self):
		# # return the frame most recently read
		# return self.frame
#
	# def stop(self):
		# # indicate that the thread should be stopped
		# self.stopped = True

# Define a thread just for marking the frames
# def FrameMark(Cond, Pin, Dur):
#     while True:
#         with Cond:
#             Cond.wait()
#             GPIO.output(Pin, GPIO.HIGH) # Turn on
#             time.sleep(Dur)
#             GPIO.output(Pin, GPIO.LOW) # Turn off
# #             print(f'{gt()} Boom!')


#%% Parameters
# FrameMarkPin = 8
# FrameMarkDur = 0.005
RecMarkPin = 10
W = 640 # image height
H = 480 # image width
fps = 75
Brightness = 60
RecDur = 2
Show = True


#%% Prepare
gt = time.perf_counter

TimeStart = time.strftime("%Y%m%d_%H%M%S")
FNameBase = f"VID_{TimeStart}"
VidName = FNameBase+'.avi'
TSName = FNameBase+'.csv'
LogName = FNameBase+'.log'
Log, TS = [], []

Log.append(f"{gt()},Run start")
MaxFrames = int(fps*RecDur)

GPIO.setwarnings(False)    # Ignore warning for now
GPIO.setmode(GPIO.BOARD)   # Use physical pin numbering
# GPIO.setup([FrameMarkPin,RecMarkPin], GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(RecMarkPin, GPIO.OUT, initial=GPIO.LOW)

# Cond = Condition()
# FrameMarkThr = Thread(target=FrameMark, args=(Cond,FrameMarkPin,FrameMarkDur,))


Log.append(f"{gt()},Setting camera...")
camera = PiCamera()
camera.resolution = (W,H)
camera.framerate = fps
# camera.brightness = 60
# camera.exposure_mode='fixedfps'
# camera.color_effects = (128,128)

rawCapture = PiRGBArray(camera, size=(W, H))
stream = camera.capture_continuous(
    rawCapture, format="bgr", use_video_port=True
)

frame = None
time.sleep(0.2) # allow the camera to warmup
rpiFPS = FPS().start()


Log.append(f"{gt()},Setting video...")
fourcc=cv2.VideoWriter_fourcc('G','R','E','Y')
out = cv2.VideoWriter(VidName, fourcc, fps, (W,H), False)


Log.append(f"{gt()},Camera opened.")
DT = gt()
TS.append("Frame,TimeStamp;Detect")

# print("Wait for pulse")
#while True:
#    #print (GPIO.input(24))
#    if(GPIO.input(24)==0):
#        break

Log.append(f"{gt()},Capturing...")
print(f"{gt()},Capturing...")
# FrameMarkThr.daemon = True
# FrameMarkThr.start()
GPIO.output(RecMarkPin, GPIO.HIGH) # Turn on Rec pin

for f,frame in enumerate(stream):
# for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
#     with Cond: Cond.notify()
    image = frame.array
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    out.write(image)

    if Show:
        cv2.imshow("Frame", image)
        key = cv2.waitKey(1) & 0xFF
        # key = cv2.waitKey(5)

    rawCapture.truncate(0)
    rpiFPS.update()
    TS.append(f"{cnt},{gt()}")

    if f > MaxFrames: break

GPIO.output(RecMarkPin, GPIO.LOW) # Turn off Rec pin
out.release()
cv2.destroyAllWindows()
stream.close()
rawCapture.close()
camera.close()
print(f"{gt()},Done.")
with open(LogName,"w") as F: F.writelines(Log)
with open(TSName,"w") as F: F.writelines(TS)
Rate = 1/np.diff([float(_.split(',')[1]) for _ in TS[1:]]).mean()

# # Here I will compress the video
# print("Converting")
# VidNameMJPG = VidName.split('.')[0]+'_MJPG.avi'
# cap = cv2.VideoCapture(VidName)
# out = cv2.VideoWriter(VidNameMJPG,cv2.VideoWriter_fourcc('M','J','P','G'), fps, (W,H))
#
# if (cap.isOpened()== False):
#     print("Error opening video file")
#
# cnt=0
# while(cap.isOpened()):
#     # Capture frame-by-frame
#     ret, frame = cap.read()
#     if ret == True:
#         out.write(frame)
#     else:
#         break
#     cnt=cnt+1
#     print (cnt)
#
# # When everything done, release the video capture object
# cap.release()
# out.release()

