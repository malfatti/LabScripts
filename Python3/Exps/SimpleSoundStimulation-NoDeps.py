#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20191209
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Quick 'n' dirty script to play sound stimulation without any calibration.
This does not depend on sciscripts package; functions were copied here.
"""

import numpy as np
import scipy.signal as ssig
import sounddevice as SD

def AudioSet(Rate=None, BlockSize=None, Channels=2, ReturnStream=None, **Kws):
    if 'system' in [_['name'] for _ in SD.query_devices()]:
        SD.default.device = 'system'
    else:
        SD.default.device = 'default'

    SD.default.channels = Channels
    if Rate: SD.default.samplerate = Rate
    if BlockSize: SD.default.blocksize = BlockSize

    if type(ReturnStream) == str:
        if ReturnStream.lower() == 'out':
            Stim = SD.OutputStream(dtype='float32')
        elif ReturnStream.lower() == 'in':
            Stim = SD.InputStream(dtype='float32')
        elif ReturnStream.lower() == 'both':
            Stim = SD.Stream(dtype='float32')
    else:
        Stim = None

    return(Stim)


def Noise(Rate, SoundPulseDur):
    print('Generating noise...')
    Noise = np.random.uniform(-1, 1, size=round(Rate*SoundPulseDur))
    Noise[-1] = 0
    Noise = np.array(Noise, dtype=np.float32)

    return(Noise)

def FilterSignal(Signal, Rate, Frequency, FilterOrder=4, Coeff='butter', Type='bandpass'):
    Data = np.zeros(Signal.shape, dtype='float32')
    if len(Signal.shape) == 2:
        for C in range(Signal.shape[1]):
            print('Filtering channel', C+1, '...')
            Data[:,C] = FilterSignal(Signal[:,C], Rate, Frequency, FilterOrder, Coeff, Type)

    else:
        if Coeff == 'butter':
            if Type not in ['bandpass', 'bandstop', 'lowpass', 'highpass']:
                print("Choose 'bandpass', 'bandstop', 'lowpass' or 'highpass'.")

            elif len(Frequency) not in [1, 2]:
                print('Frequency must have 2 elements for bandpass; or 1 element for \
                lowpass or highpass.')

            else:
                passband = [_/(Rate/2) for _ in Frequency]
                f2, f1 = ssig.butter(FilterOrder, passband, Type)
                Data = ssig.filtfilt(f2, f1, Signal, padtype='odd', padlen=0)

        elif Coeff == 'fir':
            Freqs = np.arange(1,(Rate/2)+1)
            DesiredFreqs = np.zeros(int(Rate/2))
            DesiredFreqs[min(Frequency):max(Frequency)] = 1

            o = FilterOrder + ((FilterOrder%2)*-1) +1
            a = ssig.firls(o, Freqs, DesiredFreqs, nyq=Rate/2)
            Data = ssig.filtfilt(a, 1.0, Signal, padtype='odd', padlen=0)

    return(Data)


#%%

# Set soundcard
Rate, BlockSize, Channels = 192000, 512, 1
AudioSet(Rate, BlockSize, Channels)


# Set stimulus
Freq, Dur, FullDur = [4000, 18000], 20, 90*60
FullDur = int(FullDur/Dur)
Pulse = Noise(Rate, Dur)
Pulse = FilterSignal(Pulse, Rate, Freq)
Pulse = np.concatenate([Pulse, np.zeros(int(0.5*Rate)), Pulse, np.zeros(int(4*Rate))])

# Play
for i in range(FullDur): SD.play(Pulse, blocking=True, mapping=[1,2])


