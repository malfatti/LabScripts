#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 08:35:50 2017
@author: T. Malfatti <malfatti@disroot.org>
@date: 20171110
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Calculates dose of a drug. Final volume will be animal's weight x VolAmp

Example:
    Calculate preparation of 1ml diluted xylazine stock so that:
        - the final volume injection is 10x the animal's weight;
        - the final dose is 6mg/kg;
        - the drug concentration before diluting is 20mg/ml.

    $ python3 DoseCalculator.py --dose-mgkg 6 --stock-mgml 20 --stock-ml 1 --vol-amp 10
    {'Drug1.0ml': 30.0, 'Vehicle1.0ml': 970.0}

    Therefore, diluting 30µl of xylazine at 20mg/ml  in 970µl of vehicle will
    reach 1ml of a solution that when administered a volume of 10x the animal
    weight (for example 200µl to a 20g animal) will provide 6mg/kg xylazine.

"""

from argparse import ArgumentParser
Parser = ArgumentParser()

Parser.add_argument('--dose-mgkg', help='Drug dose in mg/kg')
Parser.add_argument('--stock-mgml', help='Drug stock concentration in mg/ml')
Parser.add_argument('--stock-ml', help='Final solution volume in ml')
Parser.add_argument('--vol-amp', help='Volume amplification (dose to be applied will be "animal weight x VolAmp"', default='10')

Args = Parser.parse_args()

Dose_MgKg = float(Args.dose_mgkg)
Stock_MgMl = float(Args.stock_mgml)
Stock_Ml = float(Args.stock_ml)
VolAmp = float(Args.vol_amp)

Dose_1g_g = Dose_MgKg/1000
Dose_1g_ml = Dose_1g_g/Stock_MgMl

Dose_Final = round((Dose_1g_ml*1000*1000*Stock_Ml)/VolAmp,3)

Solution = {'Drug'+str(Stock_Ml)+'ml': Dose_Final,
            'Vehicle'+str(Stock_Ml)+'ml': (Stock_Ml*1000)-Dose_Final}
print(Solution)
print('')
