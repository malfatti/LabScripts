#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 08:35:50 2017
@author: T. Malfatti <malfatti@disroot.org>
@date: 20171110
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Timestamp-based notebook
"""

from datetime import datetime
from glob import glob

from argparse import ArgumentParser
Parser = ArgumentParser()

Date = datetime.now().strftime("%Y%m%d%H%M%S")

Parser.add_argument('-f', help='File name (defaults to "%Y%m%d%H%M%S-Notes")', default=Date+'-Notes')
Args = Parser.parse_args()

File = Args.f

Close = False
Lines = []
while not Close:
    if File in glob('*'):
        with open(File, 'r') as F: Lines = F.readlines()
    else:
        Lines = []

    print('\n\n'); print(''.join(Lines)); print('--- EOF ---\n\n')
    Line = input(': ')
    Time = datetime.now().strftime("%Y%m%d%H%M%S")
    # Time = datetime.now().replace(microsecond=0).isoformat()

    if File in glob('*'):
        with open(File, 'r') as F: Lines = F.readlines()
    else:
        Lines = []

    if Line.lower() in ['exit', 'quit', '/q']:
        Line = '\n'
        Close = True
    elif not Line:
        Line = '\n'
    elif Line[0] == '/':
        Line = Line[1:] + '\n'
    else:
        Line = Time + ' ' + Line + '\n'

    Lines.append(Line)
    with open(File, 'w') as F: F.writelines(Lines)
