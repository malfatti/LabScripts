#%% Parameters
PreviewOnly = False

AnimalName = "66825"
DataPath = "/media/pi/Backup Plus/PiRec"
RecDur = 600
BreakAt = 0
Show = True
Color = False

RecMarkPin = 10
W = 640
H = 480
fps = 60
fpsshow = 15
Brightness = 50

# ======================================================== #

#%% Run!
# from imutils.video.pivideostream import PiVideoStream
from imutils.video import FPS
from picamera.array import PiRGBArray
from picamera import PiCamera
from threading import Condition, Thread
from fractions import Fraction

import numpy as np
import os
import time
import cv2
import RPi.GPIO as GPIO

GPIO.setwarnings(False)    # Ignore warning for now
GPIO.setmode(GPIO.BOARD)   # Use physical pin numbering

class PiVideoStream:
    def __init__(
            self,
            resolution=(320, 240), framerate=32,
            brightness=40, color=True
        ):
        self.camera = PiCamera()
        self.camera.resolution = resolution
        self.camera.framerate = framerate
        time.sleep(2)
        
        ## To get best settings for environment
#         self.camera.exposure_mode='off'
#         self.camera.shutter_speed = self.camera.exposure_speed
#         g = self.camera.awb_gains
#         self.camera.awb_mode = 'off'
#         self.camera.awb_gains = g
#         print(f'Settings: shutter_speed={self.camera.shutter_speed},awb_gains={self.camera.awb_gains},brightness={self.camera.brightness}')
        
        self.camera.brightness = 50
        self.camera.exposure_mode='off'
        self.camera.shutter_speed = 16458
        self.camera.awb_mode = 'off'
        self.camera.awb_gains = (Fraction(121, 64), Fraction(367, 128))
        
        self.rawCapture = PiRGBArray(self.camera, size=resolution)
        self.color = color
        self.frame = None
        self.stopped = False
        self.stream = self.camera.capture_continuous(
            self.rawCapture,
            format="bgr", use_video_port=True
        )
        
    
    def start(self):
        Thread(target=self.update, args=()).start()
        return self
    
    def update(self):
        for f in self.stream:
#             image = f.array
#             while image is None: image = f.array
#             
#             if not self.color:
#                 image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#                 
#             self.frame = image
            self.frame = f.array
            self.rawCapture.truncate(0)
            if self.stopped:
                self.stream.close()
                self.rawCapture.close()
                self.camera.close()
                return
    
    def read(self): return self.frame
    
    def stop(self): self.stopped = True


class VideoWrite:
    """
    Class that continuously write a frame using a dedicated thread.
    """

    def __init__(self, stream, fileobj):
        self.stream = stream
        self.fileobj = fileobj
        self.stopped = False

    def start(self):
        Thread(target=self.write, args=()).start()
        return self

    def write(self):
        while not self.stopped:
            self.fileobj.write(self.stream.frame)

    def stop(self):
        self.stopped = True


class VideoProcess:
    """
    Class that continuously shows a frame using a dedicated thread.
    """

    def __init__(self, stream, color=True):
        self.stream = stream
        self.color = color
        self.stopped = False

    def start(self):
        Thread(target=self.process, args=()).start()
        return self

    def process(self):
        while not self.stopped:
            image = self.stream.read()
            while image is None:
                image = self.stream.read()
            
            if not Color:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            
            self.frame = image

    def stop(self):
        self.stopped = True


class VideoShow:
    """
    Class that continuously shows a frame using a dedicated thread.
    """

    def __init__(self, stream, fps=15):
        self.stream = stream
        self.stopped = False
        self.vsfps = fps

    def start(self):
        Thread(target=self.show, args=()).start()
        return self

    def show(self):
        while not self.stopped:            
            if not Color:
                image = cv2.cvtColor(self.stream.frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow("Frame", image)
            key = cv2.waitKey(1) & 0xFF
            time.sleep(1/self.vsfps)

    def stop(self):
        self.stopped = True


#%% Prepare
gt = time.perf_counter
GPIO.setup(RecMarkPin, GPIO.OUT, initial=GPIO.LOW)

TimeStart = time.strftime("%Y%m%d_%H%M%S")
DirFName = f"VID_{TimeStart}-{AnimalName}"
DirName = f"{DataPath}/{DirFName}"
FNameBase = f"{DirName}/{DirFName}"
VidName = FNameBase+'.avi'
TSName = FNameBase+'.csv'
LogName = FNameBase+'.log'
Log, TS = [], []

Log.append(f"{gt()},Run start at {time.strftime('%Y%m%d_%H%M%S')}\n")
MaxFrames = int(fps*RecDur)


Log.append(f"{gt()},Setting camera...\n")
VideoStream = PiVideoStream(
    resolution = (W, H),
    framerate = fps,
    brightness = Brightness,
    color = Color
).start()
time.sleep(2)

Log.append(f"{gt()},Camera opened.\n")


Log.append(f"{gt()},Capturing...\n")
print(f"{time.strftime('%Y%m%d_%H%M%S')} Capturing...")
TS.append("Frame,TimeStamp\n")

# VideoStreamProc = VideoProcess(VideoStream, Color).start()
# time.sleep(0.2)

if Show:
    VideoShowThr = VideoShow(VideoStream, fpsshow).start()

if not PreviewOnly:
    rpiFPS = FPS().start()
    
    Log.append(f"{gt()},Setting video...\n")
    os.makedirs(DirName, exist_ok=True)
    fccstr = 'MJPG' if Color else 'GREY'
    fourcc=cv2.VideoWriter_fourcc(*fccstr)
    out = cv2.VideoWriter(VidName, fourcc, fps, (W,H), isColor=Color)

    RecStart, RecTime = gt(), 0
    RecStartBreak, RecTimeBreak, BreakNo  = RecStart, 0, 1
    GPIO.output(RecMarkPin, GPIO.HIGH) # Turn on Rec pin
    while RecTime < RecDur:
        image = VideoStream.read()
        while image is None:
            image = VideoStream.read()

        if not Color:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        out.write(image)
    #     out.write(VideoStreamProc.frame)
        # if Show:
            # cv2.imshow("Frame", image)
            # key = cv2.waitKey(1) & 0xFF

        rpiFPS.update()

        trt = gt()
        RecTime = trt-RecStart
        RecTimeBreak = trt-RecStartBreak
        TS.append(f"{rpiFPS._numFrames},{gt()}\n")
        
        if BreakAt > 0:
            if RecTimeBreak > BreakAt:
                print(f"    {time.strftime('%Y%m%d_%H%M%S')} Breaking...")
                Log.append(f"{gt()},Breaking...\n")
                out.release()
                with open(TSName,"w") as F: F.writelines(TS)
                TS = ["Frame,TimeStamp\n"]
                BreakNo += 1
                RecStartBreak, RecTimeBreak = trt, 0
                VidName = f'{FNameBase}_{BreakNo}.avi'
                TSName = f'{FNameBase}_{BreakNo}.csv'
                out = cv2.VideoWriter(VidName, fourcc, fps, (W,H), isColor=Color)
                print(f"    {time.strftime('%Y%m%d_%H%M%S')} Done.")
        
        if len(TS)>2:
            ThisFPS = float(TS[-1].split(',')[1])-float(TS[-2].split(',')[1])
            if ThisFPS < 1/fps:
                time.sleep((1/fps)-ThisFPS)

    print(f"{time.strftime('%Y%m%d_%H%M%S')} Done. Cleaning...")
    Log.append(f"{gt()},Done.\n")
    if Show: VideoShowThr.stop()
    # VideoStreamProc.stop()
    rpiFPS.stop()
    out.release()
    cv2.destroyAllWindows()
    VideoStream.stop()
    time.sleep(1)
    GPIO.output(RecMarkPin, GPIO.LOW) # Turn off Rec pin
    # GPIO.cleanup()

    with open(LogName,"w") as F: F.writelines(Log)
    with open(TSName,"w") as F: F.writelines(TS)
    t = np.diff([float(_.split(',')[1]) for _ in TS[1:]])
    Rate = 1/t.mean()

    print(f"{time.strftime('%Y%m%d_%H%M%S')} Done.")
    print(f"Rate: {Rate}+-{1/t.std()}")
else:
    tans = input('Press any key to stop: ')
    if Show: VideoShowThr.stop()
    # VideoStreamProc.stop()
    cv2.destroyAllWindows()
    VideoStream.stop()

