#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20220523
@license: GNU GPLv3 <https://gitlab.com/malfatti/LabScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/LabScripts
"""

#%% Arduino recording
import matplotlib.pyplot as plt
import numpy as np

from glob import glob
from sciscripts.Exps import Arduino
from sciscripts.Analysis import Analysis

## === Experiment parameters === ##

Parameters = dict(
    AnimalName      = 'Test',
    # StimType        = ['Sound'],

    # Intensities     = [90],
    # NoiseFrequency  = [[9000, 11000]],
    # SoundPulseDur   = 60,                 # in MINUTES!

    Channels = [7,9],
    Rate = 100,

    ## === Hardware === ##
    # System = 'Jack-TestOut-TestIn',
    # System  = 'Jack-IntelOut-Marantz-IntelIn',
    Setup   = 'Treadmill',

    # Laser trigger
    # Trigger = False,
)

## === Run === ##
print('Running Arduino recording. Press Ctrl+C to stop.')
Arduino.ReadAnalogIn(**Parameters)


#%% Check rec
Files = sorted(glob('*-Test-ArduinoRec_*'))[-2:]

for f in Files:
    s = [int(_) for _ in f.split('.')[0].split('_')[-1].split('x')]
    a = np.fromfile(f, dtype=float).reshape(s)
    if 'Time' in f: Time = a
    else: Data = a

Time = Time[:,0]
DataF = Analysis.Normalize(abs(
    Analysis.FilterSignal(
        Data[:,1], Parameters['Rate'], [1,5], 2
    )
))

Peaks = Analysis.GetPeaks(DataF)['Pos']
Dist = (2*np.pi*9.5)/12
d = np.diff(Time[Peaks])[1:-1]

Fig, Axes = plt.subplots(3,1)
Axes[0].plot(Time, DataF)
Axes[0].plot(Time[Peaks], DataF[Peaks], 'x')
Axes[1].plot(Time, Data[:,0])
Axes[2].plot(Dist/(d/1000))
plt.show()