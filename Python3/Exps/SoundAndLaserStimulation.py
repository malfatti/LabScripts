#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2015
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

print('[Exps.SoundAndLaserStim] Loading dependencies...')
import numpy as np
from sciscripts.Exps import SoundAndLaserStimulation
print('[Exps.SoundAndLaserStim] Done.')


#%% Sound and Laser stimulation (Tones)
Parameters = dict(
    AnimalName          = 'Test',
    CageName          = 'B3',
    StimType            = ['Sound', 'Laser', 'SoundLaser'],

    ## === Sound === ##
    # SoundType           = 'Tone',
    # Intensities         = [70],
    # NoiseFrequency      = [int(_) for _ in np.linspace(5000, 15000, 11, dtype=int)],
    # SoundPulseNo                = 20,
    # SoundPauseBeforePulseDur    = 0,
    # SoundPulseDur               = 0.150,
    # SoundPauseAfterPulseDur     = 0.850,
    # PauseBetweenIntensities     = 0,

    SoundType           = 'Noise',
    Intensities         = [70, 40]*1,
    NoiseFrequency      = [[5000, 15000]],
    SoundPulseNo                = 20,
    SoundPauseBeforePulseDur    = 0.004,
    SoundPulseDur               = 0.003,
    SoundPauseAfterPulseDur     = 0.093,
    PauseBetweenIntensities     = 10,


    ## === Laser === ##
    # 'Sq' for square pulses, 'Sin' for sin wave
    LaserType                       = 'Sq',

    # if LaserType == 'Sq'
    LaserPauseBeforePulseDur        = 0,
    LaserPulseDur                   = 0.01,
    LaserPauseAfterPulseDur         = 0.09,
    LaserPulseNo                    = 200,
    LaserStimBlockNo                = 5,
    LaserPauseBetweenStimBlocksDur  = 10,

    # # if LaserType == 'Sin'
    # LaserDur                        = 0.1*529,
    # LaserFreq                       = 10,      # in Hz
    # LaserStimBlockNo                = 5,
    # LaserPauseBetweenStimBlocksDur  = 10,


    ## === Probe === ##
    Remapped    = True,
    Probe       = 'A16',        # None | 'A16'
    ProbeNo     = '8B95',
    Adaptor     = 'A16OM16',    # None | 'CustomAdaptor' | 'RHAHeadstage' | 'A16OM16'
    ChSpacing   = 50,


    ## === Hardware === ##
    TTLAmpF         = 1.7,
    # RecCh           = [1],
    # StimCh          = 2,
    # TTLCh           = 3,
    RecCh           = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    StimCh          = 17,
    TTLCh           = 18,

    # System = 'Jack-TestOut-TestIn',
    System          = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'UnitRec',
)

Parameters['SoundAmpF'] = {
    '-'.join([str(_) for _ in F]): np.array(Parameters['Intensities'])/max(Parameters['Intensities'])
    for F in Parameters['NoiseFrequency']
}

Stimulation, InfoFile = SoundAndLaserStimulation.Prepare(**Parameters)

#%%
from sciscripts.IO import Txt

DataInfo = Txt.Read(InfoFile)
RampStart = round(DataInfo['Audio']['SoundPauseBeforePulseDur']*DataInfo['Audio']['Rate'])
RampEnd = RampStart + round(DataInfo['Audio']['SoundPulseDur']*DataInfo['Audio']['Rate'])
RampDur = round(0.01*DataInfo['Audio']['Rate'])

Ramp = np.ones(Stimulation['Sound'].shape[0], dtype=float)
Ramp[RampStart:RampStart+RampDur] = np.linspace(0,1,RampDur)
Ramp[RampEnd-RampDur:RampEnd] = np.linspace(1,0,RampDur)

for F in range(Stimulation['Sound'].shape[3]):
    for A in range(Stimulation['Sound'].shape[2]):
        Stimulation['Sound'][:,1,A,F] *= Ramp


#%% Run
SoundAndLaserStimulation.Play(Stimulation, InfoFile, ['Sound'], DV='Out')





#%% Sound and Laser stimulation (Noise)

Parameters = dict(
    AnimalName          = 'B3',
    CageName          = 'B3',
    StimType            = ['Sound'],

    ## === Sound === ##
    SoundType           = 'Noise',
    Intensities         = [80, 75, 70, 65, 60, 55, 50, 45],
    NoiseFrequency      = [
        [8000, 10000],
        [9000, 11000],
        [10000, 12000],
        [12000, 14000],
        [14000, 16000]
    ],

    # Fill all durations in SECONDS!
    SoundPulseNo                = 25**2,
    SoundPauseBeforePulseDur    = 0.004,
    SoundPulseDur               = 0.003,
    SoundPauseAfterPulseDur     = 0.093,
    PauseBetweenIntensities     = 10,

    # Intensities         = [80]*5,
    # NoiseFrequency      = [[5000, 15000]],
    # SoundPulseNo                = 200,


    ## === Laser === ##
    # # 'Sq' for square pulses, 'Sin' for sin wave
    # LaserType                       = 'Sq',

    # # if LaserType == 'Sq'
    # LaserPauseBeforePulseDur        = 0,
    # LaserPulseDur                   = 0.01,
    # LaserPauseAfterPulseDur         = 0.09,
    # LaserPulseNo                    = 200,

    # # if LaserType == 'Sin'
    # LaserDur                        = 0.1*529,
    # LaserFreq                       = 10,      # in Hz

    # LaserStimBlockNo                = 5,
    # LaserPauseBetweenStimBlocksDur  = 10,


    ## === Probe === ##
    # Remapped    = True,
    # Probe       = 'A16',        # None | 'A16'
    # ProbeNo     = '8B95',
    # Adaptor     = 'A16OM16',    # None | 'CustomAdaptor' | 'RHAHeadstage' | 'A16OM16'
    # ChSpacing   = 50,


    ## === Hardware === ##
    TTLAmpF         = 1.7,
    RecCh           = [1],
    StimCh          = 2,
    TTLCh           = 3,
    # RecCh           = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    # StimCh          = 17,
    # TTLCh           = 18,

    # System = 'Jack-TestOut-TestIn',
    System          = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'UnitRec',
)

Stimulation, InfoFile = SoundAndLaserStimulation.Prepare(**Parameters)


