from IO import IO
from glob import glob
import numpy as np

#%%
Folders = sorted(glob('/home/malfatti/Documents/PhD/Data/Prev*/*UnitRec'))
Folders += sorted(glob('/home/malfatti/Documents/PhD/Data/Rec*/*UnitRec'))
ToRepeat = []

for Folder in Folders:
    SubFolders = sorted(glob(Folder+'/20??-*'))
    SubFolders = [_ for _ in SubFolders if 'impedance' not in _.lower()]

    for SubFolder in SubFolders:
        File = glob(SubFolder+'/settings.xml')[0]
        ChNo = IO.OpenEphys.GetChNoAndProcs(File)[0]
        Rate = IO.OpenEphys.SettingsXML.GetSamplingRate(File)

        if ChNo != 18 or Rate != 30000:
            print(SubFolder)
            ToRepeat.append(SubFolder)

ToRepeat = ['/'.join(_.split('/')[:-1]) for _ in ToRepeat]
ToRepeat = np.unique(ToRepeat)

print('Done')
