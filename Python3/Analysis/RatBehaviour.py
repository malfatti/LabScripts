#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Rat Behaviour analysis
- Vocalizations are expected to be the output of D. Laplagne's code
- Tracking data is expected to be the output of Icy Mouse Profiler Tracker
"""

#=== Plot Vocs and Behaviours
print('Loading dependencies...')
import numpy as np
import os
from glob import glob
from scipy import io
from xml.etree import ElementTree

from sciscripts.Analysis import Vocalizations
from sciscripts.Analysis.Plot import Plot
from sciscripts.IO import IO, MouseProfilerTracker

plt = Plot.Return('plt')
Ext = ['pdf', 'svg']

Groups = ['G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10']
Pairs = ['MSFS', 'MSFD', 'MDFS', 'MDFD']
Classes = ['22kHz', '50kHz']

OFCamFPS = 23
Px2M = 0.43/128
rR = 0.50
r = round(rR/Px2M)
SpeakerPos = {'X': 532.5, 'Y': 1014.75}

DataPath = os.environ['HOME']'/Documents/Data/VSLB'
AnalysisPath = os.environ['CALIGOPATH']+'/ChandraNalaar/Documents/Analysis/VSLB'

print('Done')


#%% Separate 22kHz and 50kHz vocs
Files = sorted(glob(DataPath+'/**/T000*.mat', recursive=True))
# Files = sorted(glob(DataPath+'/**/T000*.dat', recursive=True))
PB = [
    F for F in Files
    if 'back' in F
    or 'G3-OF' in F
    or 'OpenFieldPB' in F
]
for F in PB: del(Files[Files.index(F)])

Recs = IO.Txt.Read(DataPath+'/Recs.txt')
VocsSorted = {}; Vocs = {}

for File in Files:
    print(File)
    Rec = '/'.join(File.split('/')[-2:])
    if Rec not in Recs: continue

    Group, Pair = Recs[Rec]
    try: Setup = File.split('/')[-2].split('-')[2]
    except IndexError: Setup = 'on'

    if Group not in Groups: Group = 'G3'
    if Setup[-2:] == 'on': Setup = 'SI'
    elif Setup[-2:] == 'ld': Setup = 'OF'
    elif Setup[-4:] == 'back': Setup = 'OFPB'
    else:
        print('Setup is now', Setup)
        Setup = input('What setup is this? ')

    G = Groups.index(Group); P = Pairs.index(Pair)

    if Setup not in VocsSorted: VocsSorted[Setup] = {}
    if Group not in VocsSorted[Setup]: VocsSorted[Setup][Group] = {}

    VocsSorted[Setup][Group][Pair] = Vocalizations.USVSort(File)

    if Setup not in Vocs: Vocs[Setup] = {}
    for Class in Classes:
        if Class not in Vocs[Setup]:
            Vocs[Setup][Class] = np.zeros((len(Groups), len(Pairs)))

        Vocs[Setup][Class][G,P] = len(VocsSorted[Setup][Group][Pair][Class])


IO.Txt.Write(Vocs, '/home/ciralli/Caligo/ChandraNalaar/Documents/Analysis/VSLB/Vocs.dict')
IO.Bin.Write(VocsSorted, '/home/ciralli/Caligo/ChandraNalaar/Documents/Analysis/VSLB/VocsSorted')

#%% OF Behaviours
# BehavioursFile = 'Analysis/G32G7-BehavioursPB.mat'
# Behaviours = io.loadmat(BehavioursFile, squeeze_me=True, struct_as_record=False)
BehavioursFile = AnalysisPath+'/Tracking/G32G7-BehavioursPB'
Behaviours = IO.Bin.Read(BehavioursFile)

Keys = list(Behaviours.keys())
for K in Keys:
    if K[0] == '_': del(Behaviours[K])

Groups = Behaviours.pop('Cols')
Pairs = Behaviours.pop('Lines')

for B, Behaviour in Behaviours.items():
    if B[:2] == 'OF':
        if B[:4] == 'OFPB': FigName = 'G32G7-OFPB-'+B
        else: FigName = 'G32G7-OF-'+B
    else: FigName = 'G32G7-SI-'+B

    if B == 'Darts': B = 'Dart/Hop'
    Fig, Ax = ScatterMean(Behaviour, '', 'Pairs', 'Number of events per 20min', Pairs, FigName, 200, Save=True)
plt.show()


#%% Icy Mice Profiler analysis - XML to Bin
Recs = IO.Txt.Read(AnalysisPath+'/Recs.txt')
Files = sorted(glob(AnalysisPath+'/Tracking/*/*/*allEventsExported.xml'))
FilesPB = [_ for _ in Files if 'OpenFieldPB' in _]
Files = [_ for _ in Files if _ not in FilesPB]
FilesInverted = [_.replace('/Tracking/','/TrackingInverted/') for _ in Files]
FilesInvertedPB = [_.replace('/Tracking/','/TrackingInverted/') for _ in FilesPB]
CoordFiles = ['.'.join(F.split('.')[:-2]) for F in Files]
CoordFilesPB = ['.'.join(F.split('.')[:-2]) for F in FilesPB]

for Set in [
    [Files, CoordFiles, FilesInverted, 'OF'],
    [FilesPB, CoordFilesPB, FilesInvertedPB, 'OFPB'],
    ]:

    Data = MouseProfilerTracker.TrackingRead(Set[0], Set[1], Recs, Coords=True)
    DataInverted = MouseProfilerTracker.TrackingRead(Set[2], Set[1], Recs, Coords=False)

    for G, Group in DataInverted.items():
        for P, Pair in Group.items():
           Data[G][P]['Behaviours']['BFollowA'] = Pair['Behaviours']['AFollowB']

    if Set[3] == 'OFPB':
        for G, Group in Data.items():
            for P, Pair in Group.items():
                for A, Animal in Pair['Coords'].items():
                    Coords = np.vstack((Animal['bodyx'], Animal['bodyy'])).T
                    Events = np.zeros(Coords.shape[0])
                    Events[np.where(((Coords[:,0] - SpeakerPos['X'])**2 + (Coords[:,1] - SpeakerPos['Y'])**2)**0.5 < r)] = 1
                    if Events[0] == 1: Events[0] = 0; Events[1] = 1

                    Events = np.diff(Events)
                    Events = np.hstack((np.argwhere(Events == 1), np.argwhere(Events == -1)))

                    Data[G][P]['Behaviours'][A[-1]+'GoToSpeaker'] = Events

        # for G, Group in Data.items():
            # for P, Pair in Group.items():
                # Data[G][P]['Behaviours']['APlusBGoToSpeaker'] = np.vstack((
                    # Pair['Behaviours']['AGoToSpeaker'],
                    # Pair['Behaviours']['BGoToSpeaker']
                # ))

    for G, Group in Data.items():
        for P, Pair in Group.items():
            Data[G][P]['BehavioursDur'] = {
                B: np.diff(Behaviour, axis=1).ravel()/OFCamFPS
                if len(Behaviour) else np.array([])
                for B,Behaviour in Pair['Behaviours'].items()
            }

    IO.Bin.Write(Data, f'{AnalysisPath}/{Set[3]}Behaviours')

print('Done.')


#%% Add BehavioursDur to previous group
for Setup in ['OF', 'OFPB']:
    for Group in Groups[:-3]:
        if Group == 'G3' and Setup == 'OFPB': continue

        for Pair in Pairs:
            Data = IO.Bin.Read(f'{AnalysisPath}/{Setup}Behaviours/{Group}/{Pair}/Behaviours')[0]

            BehavioursDur = {}
            for B,Behaviour in Data.items():
                if len(Behaviour):
                    if len(Behaviour.shape) == 1:
                        BehavioursDur[B] = np.diff(Behaviour).ravel()/OFCamFPS
                    else:
                        BehavioursDur[B] = np.diff(Behaviour, axis=1).ravel()/OFCamFPS
                else:
                    BehavioursDur[B] = np.array([])

            IO.Bin.Write(BehavioursDur, f'{AnalysisPath}/{Setup}Behaviours/{Group}/{Pair}/BehavioursDur')


#%%
