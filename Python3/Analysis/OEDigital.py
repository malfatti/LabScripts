def GetEventEdges(EventsDict, Ch, Proc, Rec, Edge='rise'):
    if Edge.lower() == 'rise': Id = 1
    elif Edge.lower() == 'fall': Id = 0
    else: print('Edge should be "rise" or "fall"')

    Events = EventsDict['sampleNum'][(EventsDict['channel'] == Ch) *
                                     (EventsDict['recordingNumber'] == Rec) *
                                     (EventsDict['nodeId'] == Proc) *
                                     (EventsDict['eventId'] == Id) *
                                     (EventsDict['eventType'] == 3)]

    return(Events)


def GetTTLInfo(Events, EventRec, TTLCh):
    print('Get TTL data...')
    EventID = Events['TTLs']['user_data']['eventID']
    EventCh = Events['TTLs']['user_data']['event_channels']
    EventSample = Events['TTLs']['time_samples']

    TTLRecs = np.nonzero(np.bincount(EventRec))[0]
    TTLRecs = ["{0:02d}".format(_) for _ in TTLRecs]
    TTLsPerRec = {Rec: [EventSample[_] for _ in range(len(EventRec))
                         if EventRec[_] == int(Rec)
                         and EventCh[_] == TTLCh-1
                         and EventID[_] == 1]
                  for Rec in TTLRecs}

    return(TTLsPerRec)



