#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20190819
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import numpy as np
import os
from glob import glob
from imp import load_source

from sciscripts.IO import IO, Klusta
from sciscripts.Analysis import Analysis, ABRs
from sciscripts.Analysis.ABRs import GetTTLs
from sciscripts.Analysis.Plot import Plot
plt = Plot.Return('plt')


NoTTLsFile = os.environ['ANALYSISPATH']+'/Experiments/NoTTLRecs.dict'


#%% Rewrite huge .dat
Files = [
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_13-56-46_Recovery_08-1416/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_11-52-19_Recovery_16-1214/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-15-24_Recovery_05-0810/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_13-56-46_Recovery_08-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-15-24_Recovery_05-0810/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_11-52-19_Recovery_16-1214/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-27-14_Recovery_05-1012/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-24-46_Recovery_12-0810/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-10-45_Recovery_08-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-03-06_Recovery_16-1416/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-27-14_Recovery_05-1012/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-10-45_Recovery_08-0911/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-14-17_Recovery_16-1012/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-22-46_Recovery_08-1012/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-37-02_Recovery_08-0810/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_15-04-25_Recovery_05-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-06-13_Recovery_12-1214/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-41-47_Recovery_16-0810/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-48-53_Recovery_08-1214/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_15-04-25_Recovery_05-0911/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-41-47_Recovery_16-0810/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-48-53_Recovery_08-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-06-13_Recovery_12-1214/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-16-48_Recovery_12-1012/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-16-48_Recovery_12-1012/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-27-44_Recovery_12-0911/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-27-44_Recovery_12-0911/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-29-40_Recovery_11-1012/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-29-40_Recovery_11-1012/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-40-48_Recovery_11-0810/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat'
]

for F,File in enumerate(Files):
#     if F < 26: continue
    Data = np.memmap(File, dtype='int16')
    Start = np.where((Data != 0))[0][0]
    Start = (Start//3)*3
    Data = Data[Start:]
    DatFile = 'DATFix/'+'/'.join(File.split('/')[6:])
    os.makedirs('/'.join(DatFile.split('/')[:-1]), exist_ok=True)
    with open(DatFile, 'wb') as F: Data.tofile(F)

    del(Data)


#%% Rewrite kwik with more recordings than expected
Folder = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_11-04-17_PreventionControl_03-09_11Kwik'

a,b = IO.DataLoader(Folder, 'bits')

Done = []
for P,Proc in a.items():
    Recs = [[R, Rec.shape[0]] for R,Rec in Proc.items()]
    Threshold = max([_[1] for _ in Recs])/2
    Recs = [_ for _ in Recs if _[1] > Threshold]

    for R,Rec in Recs:
        File = 'experiment1_'+P+'_'+str(len(Done))+'.dat'
        File = Folder+'/'+File
        File = File.replace('Kwik', '')

        print(Proc[R].dtype)
        print(R)
        print(File)
        print()

        IO.Bin.Write(Proc[R], File)

        Done.append(R)



#%% Update dicts
Dicts = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*UnitRec/*dict'))

for Dict in Dicts:
    Info = IO.Txt.Read(Dict)
    Info = IO.Txt.Dict_OldToNew(Info)
    IO.Txt.Write(Info, Dict)


#%% Update PRMs
PRMs = sorted(glob('/run/media/malfatti/Malfatti1TB4/Analysis/[Rec,Prev]*/*UnitRec/*/*prm'))

for PRMFile in PRMs:
    PRM = load_source('', PRMFile)
    Info = IO.Txt.Dict_OldToNew(PRM.DataInfo)
    PRM.DataInfo = Info

    Txt = []
    Attrs = [_ for _ in dir(PRM) if '__' not in _]
    for Attr in Attrs:
        if '__' in Attr: continue

        Txt.append(Attr + ' = ' + IO.Txt.Print(getattr(PRM, Attr))+'\n')
        Txt.append('\n')

    with open(PRMFile, 'w') as F:
        F.writelines(Txt)



#%% Add RawData to PRMs
Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*UnitRec'))

for Folder in Folders:
    ExpFolder = os.environ['ANALYSISPATH']+'/'+'/'.join(Folder.split('/')[-2:]) + '/KlustaFiles'
    os.makedirs(ExpFolder, exist_ok=True)

    ExpName = Folder.split('/')[-2]
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    ExpGroups = [Exps]
    if Folder+'/ExpGroups' in glob(Folder+'/*'):
        ExpGroups = IO.Txt.Read(Folder+'/ExpGroups')
        ExpGroups = [[Folder+'/'+_ for _ in F] for F in ExpGroups]

    for Es, Exps in enumerate(ExpGroups):
        FilesPrefix = ExpFolder+'/'+ExpName+'_Exp' + "{0:02d}".format(int(Es))

        raw_data_files = []
        DataInfo = glob(Folder + '/*.dict')[0]
        DataInfo = IO.Txt.Read(DataInfo)

        if 'Probe' in DataInfo.keys():
            if 'ChSpacing' in DataInfo['Probe']:
                ProbeSpacing = DataInfo['Probe']['ChSpacing']

            if DataInfo['Probe']['Remapped']:
                PrbFile = os.environ['SCRIPTSPATH'] + \
                          '/Python3/Klusta/A16-'+str(ProbeSpacing)+'.prb'
            else:
                PrbFile = FilesPrefix+'.prb'
        else:
            print('No probe info saved. Skip remap...')
            PrbFile = os.environ['SCRIPTSPATH'] + '/Python3/Klusta/A16-50.prb'

        for E, Exp in enumerate(Exps):
            FilesExt = [F.split('.')[-1] for F in glob(Exp+'/*.*')]

            ChNo, Procs = IO.OpenEphys.SettingsXML.GetRecChs(Exp+'/settings.xml')
            if len(ChNo) > 1:
                Proc = [K for K,V in Procs.items() if 'FPGA'in V][0]
            else:
                Proc = list(ChNo.keys())[0]
            ChNo = len(ChNo[Proc])


            if FilesExt == ['xml'] or 'dat' in FilesExt:
                Files = sorted(glob(Exp+'/**/*.dat', recursive=True))
                if 'experiment' in Files[0]:
                    Files = [_ for _ in Files if Proc in _.split('_')[-2]]
                raw_data_files += Files

                Rate = IO.OpenEphys.SettingsXML.GetSamplingRate(Exp+'/settings.xml')
                DType = 'int16'

            else:
                print('Loading', Exp, '...')
                Data, Rate = IO.DataLoader(Exp, 'Bits')
                # RecChMap = (np.arange(16)).tolist()+[DataInfo['DAqs']['StimCh']-1,DataInfo['DAqs']['TTLCh']-1]

                for R, Rec in sorted(Data[Proc].items(), key=lambda i: int(i[0])):
                    DataFile = ''.join([ExpName, '_Exp', "{0:02d}".format(int(Es)),
                                       '-', "{0:02d}".format(int(E)), '_Rec',
                                       "{0:02d}".format(int(R))])
                    RawDataInfo = {'Rate': Rate[Proc], 'Shape': Rec.shape, 'DType': str(Rec.dtype)}
                    raw_data_files.append(ExpFolder+'/'+DataFile+'.dat')

                ChNo = RawDataInfo['Shape'][1]
                Rate = int(RawDataInfo['Rate'])
                DType = RawDataInfo['DType']
                del(Data)

        DataInfo['RawData'] = Exps
        Klusta.PrmWrite(FilesPrefix+'.prm', ExpName+'_Exp' + "{0:02d}".format(int(Es)),
                        PrbFile, raw_data_files, Rate, ChNo, DType,
                        DataInfo=DataInfo)


#%% Rewrite wrong kwik data to binary (forgot to press '+' on OE)
a,b = IO.DataLoader('/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-08_10Kwik', 'bits')

for P,Proc in a.items():
    for R,Rec in Proc.items():
        if int(R) < 5:
            f = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-08_10'
        else:
            f = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-14_16'
            R = str(int(R)-5)

        print(Rec.dtype)
        print(f)
        print(int(R))
        print()

        IO.Bin.Write(Rec, f+'/experiment1_'+P+'_'+R+'.dat')



#%% Check TTLs
def TTLsReplace(Array, TTLsToFix):
    """
    Replace bad timestamps with one of the known good ones.
    """
    for TTL in TTLsToFix:
        nInd = np.random.randint(1, 100)
        while nInd == TTL: nInd = np.random.randint(0, 100)
        while nInd >= len(Array): nInd = np.random.randint(0, 100)

        print('TTL', str(TTL), 'was replaced by', str(nInd))
        Array[TTL] = Array[nInd]

    return(Array)


def WriteRecAndTTLCh(Folders):
    """
    Check if timestamps can be detected for each folder.

    Sometimes, things go wrong. A cable may have been disconnected right
    before/during recordings, or the timestamp-marking system may have failed
    you.

    This function will interactively work with you to find your beloved (and
    missing) timestamps, then write it to a file.

    Parameters
    ----------

    Folders: list
        List of folders containing data loadable by
        `sciscripts.IO.IO.DataLoader`.

    """
    for Folder in Folders:
        print(Folder)
        Data, Rate = IO.DataLoader(Folder, Unit='uV', ChannelMap=[])
        if len(Data.keys()) == 1: Proc = list(Data.keys())[0]
        Data, Rate = Data[Proc]['0'], Rate[Proc]['0']

        InfoFile = glob(Folder+'/../*.dict')[0]

        for R, Rec in Data.items():
            print(R)
            Info = IO.Txt.Read(InfoFile)
            TTLs = []
            if Info['DAqs']['TTLCh'] <= Rec.shape[1]:
                TTLs = Analysis.QuantifyTTLs(Rec[:,Info['DAqs']['TTLCh']-1])

            if len(TTLs) < 100 or len(TTLs) > Info['Audio']['SoundPulseNo']+100:
                print('TTLs:', len(TTLs))
                while len(TTLs) < 100 or len(TTLs) > Info['Audio']['SoundPulseNo']+100:
                    Break = ''
                    Chs = [Rec[:int(Rate/2),Ch] for Ch in range(Rec.shape[1])]
                    Plot.RawCh(Chs, Lines=len(Chs), Cols=1, Save=False)

                    TTLCh = input('TTLCh: ')
                    RecCh = input('RecCh (space separated): ')
                    TTLCh = int(TTLCh)
                    RecCh = [int(_) for _ in RecCh.split(' ')]
                    Break = input('Break [y/N]? ')

                    if Break.lower() in ['y', 'yes']: break
                    TTLs = Analysis.QuantifyTTLs(Rec[:,TTLCh-1])
                print('TTLs:', len(TTLs))

                if Break.lower() not in ['y', 'yes']:
                    if RecCh != Info['DAqs']['RecCh']:
                        Info['DAqs']['RecCh'] = RecCh
                        IO.Txt.Write(Info, InfoFile)
                        IO.Bin.Write(Rec[:,RecCh], Folder+'/RecCh_'+R+'.dat')


                    Info['DAqs']['TTLCh'] = TTLCh
                    IO.Txt.Write(Info, InfoFile)
                    IO.Bin.Write(TTLs, Folder+'/TTLs_'+R+'.dat')

    return(None)


def _PrintOptions():
    """
    Helper function for WriteRecAndTTLCh():
    """
    print('')
    print('This plot shows the first 0.5s of this recording.')
    print('What to do?')
    print('1) Decrease TTL Threshold')
    print('2) Change TTL channel')
    print('3) Decrease TTL Threshold and change TTL channel')
    print('4) Choose a startpoint for TTLs')
    print('5) Plot TTLCh and TTL threshold')
    print('6) Change ABR and TTL channels')
    print('7) Ignore PulseNo restritions')
    print('8) Report broken rec')
    print('9) Abort')
    print('')

    return(None)


def FixTTLs(Data, Rate, DataInfo, Freq, Rec):
    """
    Under dev.
    """
    NoTTLRecs = IO.Txt.Read(NoTTLsFile)
    F = DataInfo['InfoFile'].split('/')[-1].split('.')[0]
    TTLCh = DataInfo['DAqs']['TTLCh']
    DataCh = DataInfo['DAqs']['RecCh']


    WhatToDo = None; IgnoreRestritions = False
    if F in NoTTLRecs:
        if Freq in NoTTLRecs[F]:
            if Rec in NoTTLRecs[F][Freq]:
                WhatToDo = NoTTLRecs[F][Freq][Rec]['WhatToDo']
                Std = NoTTLRecs[F][Freq][Rec]['Std']
                SampleStart = NoTTLRecs[F][Freq][Rec]['SampleStart']

                if NoTTLRecs[F][Freq][Rec]['TTLCh']:
                    TTLCh = NoTTLRecs[F][Freq][Rec]['TTLCh']

                if NoTTLRecs[F][Freq][Rec]['DataCh']:
                    DataCh = NoTTLRecs[F][Freq][Rec]['DataCh']

                if WhatToDo == '6': ChangedDataCh = True


    NoChoice = False
    if not WhatToDo:
        NoChoice = True
        if 'Plot' not in locals():
            from Analysis.Plot import Plot
            plt = Plot.Return('plt')

        print('DataCh:', DataCh)
        print('TTLCh:', TTLCh)
        print('')
        _PrintOptions()

        Chs = Data[:int(Rate/2),:]

        if DataInfo['InfoFile'].split('/')[-1].split('-')[0] in ['20170623145925', '20170623162416']:
            Chs[:,-1] = Analysis.FilterSignal(Chs[:,-1], Rate, [8000, 14900])

        Fig, Axes = plt.subplots(1,2,figsize=(10,5), sharex=True)
        Axes[0] = Plot.AllCh(Chs, lw=0.6, Ax=Axes[0])
        Axes[1].plot(Analysis.GetInstFreq(Data[:int(Rate/2),TTLCh-1], Rate))
        plt.show()

        WhatToDo = input(': ')

        if F not in NoTTLRecs: NoTTLRecs[F] = {}
        if Freq not in NoTTLRecs[F]: NoTTLRecs[F][Freq] = {}
        if Rec not in NoTTLRecs[F][Freq]: NoTTLRecs[F][Freq][Rec] = {}

        NoTTLRecs[F][Freq][Rec]['WhatToDo'] = WhatToDo
        for K in ['SampleStart', 'Std', 'TTLCh', 'DataCh']:
            if K not in NoTTLRecs[F][Freq][Rec].keys():
                NoTTLRecs[F][Freq][Rec][K] = None

        Std = NoTTLRecs[F][Freq][Rec]['Std']
        SampleStart = NoTTLRecs[F][Freq][Rec]['SampleStart']
        ChangedDataCh = False


    if WhatToDo == '1':
        if not Std or NoChoice:
            Std = input('How many std above the mean should the threshold be? ')
            Std = float(Std)

        TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1], Std)

        NoTTLRecs[F][Freq][Rec]['Std'] = Std
        IO.Txt.Write(NoTTLRecs, NoTTLsFile)

    elif WhatToDo == '2':
        if not TTLCh or NoChoice:
            TTLCh = input('TTL channel: '); TTLCh = int(TTLCh)

        TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1])

        NoTTLRecs[F][Freq][Rec]['TTLCh'] = TTLCh

    elif WhatToDo == '3':
        if not Std or NoChoice:
            Std = input('How many std above the mean should the threshold be? ')
            Std = float(Std)

        if not TTLCh:
            TTLCh = input('TTL channel: '); TTLCh = int(TTLCh)
            TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1], Std)

        NoTTLRecs[F][Freq][Rec]['Std'] = Std
        NoTTLRecs[F][Freq][Rec]['TTLCh'] = TTLCh
        IO.Txt.Write(NoTTLRecs, NoTTLsFile)

    elif WhatToDo == '4':
        if not SampleStart:
            SampleStart = input('Sample of the rising edge of the 1st pulse: ')
            SampleStart = int(SampleStart)

        TTLs = Analysis.GenTTLsRising(Rate, DataInfo['Audio']['SoundPulseDur'],
                                  DataInfo['Audio']['SoundPauseBeforePulseDur'],
                                  DataInfo['Audio']['SoundPauseAfterPulseDur'], SampleStart,
                                  DataInfo['Audio']['SoundPulseNo'])

        NoTTLRecs[F][Freq][Rec]['SampleStart'] = SampleStart

    elif WhatToDo == '5':
        Std = NoTTLRecs[F][Freq][Rec]['Std']

        Threshold = Analysis.GetTTLThreshold(Data[:,TTLCh-1], Std)
        Plot.TTLCh(Data[:,TTLCh-1], Std, Threshold)
        TTLs = []

    elif WhatToDo == '6':
        if not ChangedDataCh:
            NewABRCh = input('ABR channels (comma separated): ')
            NewABRCh = [int(_) for _ in NewABRCh.split(',')]
            TTLCh = input('TTL channel: '); TTLCh = int(TTLCh)

            TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1])

            NoTTLRecs[F][Freq][Rec]['DataCh'] = NewABRCh
            NoTTLRecs[F][Freq][Rec]['TTLCh'] = TTLCh

            Info = {'ABRCh': NewABRCh, 'TTLCh': TTLCh}
            print(len(TTLs), 'TTLs')
            print('ABR Channels changed. Restarting calculations...')
            print('')
        else:
            TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1])

    elif WhatToDo == '7':
        Std = NoTTLRecs[F][Freq][Rec]['Std']

        TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1], Std)
        IgnoreRestritions = True

    elif WhatToDo == '8':
        TTLs = []
        NoTTLRecs[F][Freq][Rec]['BrokenRec'] = True
        IO.Txt.Write(NoTTLRecs, NoTTLsFile)
        return('Broken')

    else:
        print('Aborted.')
        raise(SystemExit)


    if not IgnoreRestritions:
        if len(TTLs) > DataInfo['Audio']['SoundPulseNo']+4 or len(TTLs) < 100:
            TTLs = []

    if len(TTLs) == 0:
        NoTTLRecs[F][Freq][Rec]['WhatToDo'] = None
        IO.Txt.Write(NoTTLRecs, NoTTLsFile)
    else:
        IO.Txt.Write(NoTTLRecs, NoTTLsFile)
        if WhatToDo == '6'and not ChangedDataCh: return(Info)

    print(len(TTLs), 'TTLs after fix.')
    return(TTLs)


def GetTTLs(Data, Rate, DataInfo, DataFolder, R=None, Freq=None):
    """
    Under dev.
    """
    TTLCh = DataInfo['DAqs']['TTLCh']
    ABRCh = DataInfo['DAqs']['RecCh']

    if TTLCh > Data.shape[1]:
        print('TTLCh > ChNo; replaced by -1.')
        TTLCh = 0

    print('TTL mean and max:', np.mean(Data[:,TTLCh-1]), np.max(Data[:,TTLCh-1]))
    TTLs = Analysis.QuantifyTTLs(Data[:,TTLCh-1])
    if len(TTLs) > DataInfo['Audio']['SoundPulseNo']+10 or len(TTLs) < 100:
        TTLs = []

    if max(ABRCh) > Data.shape[1]:
        print(''); print('Wrong ABR Channels!'); print('')

    TTLFile = glob(DataFolder+'/Rec_'+R+'_TTLs.TTLs')
    if len(TTLFile):
        TTLs = IO.Bin.Read(TTLFile[0])[0]

        if TTLs[0] == 'Broken':
            print('Exp', DataFolder.split('/')[-1], 'Rec', R, 'is broken.')
            return('Broken')

    if not len(TTLs):
        print('Fixing TTLs...')
        while len(TTLs) == 0:
            TTLs = FixTTLs(Data, Rate, DataInfo, Freq, R)

        if type(TTLs) == dict:
            DI = deepcopy(DataInfo)
            DI['DAqs']['RecCh'] = TTLs['ABRCh']
            DI['DAqs']['TTLCh'] = TTLs['TTLCh']

            TTLs = []
            while len(TTLs) == 0:
                TTLs = FixTTLs(Data, Rate, DI, Freq, R)

            Chs = [_-1 for _ in DI['DAqs']['RecCh']]

            IO.Bin.Write(Data[:,Chs], DataFolder+'/Rec_'+R+'_ABRs.ABRs')

        if type(TTLs) == str:
            if TTLs == 'Broken':
                print('Exp', DataFolder.split('/')[-1], 'Rec', R, 'is broken.')
                IO.Bin.Write(np.array(['Broken']), DataFolder+'/Rec_'+R+'_TTLs.TTLs')
                return(TTLs)

        if type(TTLs) == list: TTLs = np.array(TTLs)
        IO.Bin.Write(TTLs, DataFolder+'/Rec_'+R+'_TTLs.TTLs')
        print('Done')

    print(len(TTLs), 'TTLs')
    return(TTLs)



Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-ABRs') + glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-UnitRec'))
PlotRange = np.arange(0,15000,6)

for F,Folder in enumerate(Folders):
    if Folder not in [
        # '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180130-RecoveryControl_03-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_01-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_02-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_04-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_05-UnitRec'
    ]: continue

    ExpName = Folder.split('/')[-2]
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')[0]
    DataInfo = IO.Txt.Read(DataInfo)
    # ChannelMap = DataInfo['DAqs']['RecCh']+[DataInfo['DAqs']['TTLCh']]
    # DataInfo['DAqs']['RecCh'] = list(np.arange(len(ChannelMap)-1)+1)
    # DataInfo['DAqs']['TTLCh'] = len(ChannelMap)

    for E, Exp in enumerate(Exps):
        if 'asal' in Exp or 'aselin' in Exp: continue
        if 'asal' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz'] or 'aselin' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']:
            continue

        print('Loading', Exp, '...')
        # Data, Rate = IO.DataLoader(Exp, 'Bits', ChannelMap)
        Data, Rate = IO.DataLoader(Exp, 'Bits')
        # Plot.AllCh(Data[P]['0'][PlotRange,:], lw=0.8, AxArgs={'title':Exp.split('/')[-2]+'_'+Exp.split('/')[-1].split('_')[-1]+'_'+P+'_'+R})


        for P,Proc in Data.items():
            if Data[P][list(Data[P].keys())[0]].shape[1] in [1,16]: continue

            for R,Rec in Proc.items():
                # if R == '4': continue
                Rec = Rec.astype('float32')
                if Rec.shape[0] < max(PlotRange):
                    print('Rec', R, 'is too short, very likely it is broken.')
                    continue

                Freq = DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']
                print('Folder', F, 'Exp', E, 'Rec', R, 'Freq', Freq)

                ## Load Data file if present
                ABR = glob(Exp+'/Rec_'+R+'ABRs.ABRs')
                if len(ABR):
                    Rec[:,DataInfo['DAqs']['RecCh']] = IO.Bin.Read(ABR[0])[0]
                ## ====

                # TTLs = Analysis.QuantifyTTLs(Rec[:,DataInfo['DAqs']['TTLCh']-1])
                TTLs = GetTTLs(Rec, Rate[P], DataInfo, Exp, True, R, Freq+'_'+"{0:02d}".format(int(E)))

                ## Check if rec is broken
                if type(TTLs) == str:
                    if TTLs == 'Broken': continue
                ## ====

                ## Visual double-check
                if len(TTLs):
                    RecTTLs = TTLs[(TTLs>PlotRange[0])*(TTLs<PlotRange[-1])]-PlotRange[0]
                else:
                    RecTTLs = [0]

                # if not len(RecTTLs):
                #     plt.plot(Analysis.Normalize(Rec[:,DataInfo['DAqs']['RecCh']]))
                #     for TTL in TTLs: plt.axvspan(TTL, TTL+0.003*Rate[P], color='r', alpha=0.3)
                #     plt.plot(Rec[:,-1])
                #     plt.plot([0,len(Rec)], [-232]*2)
                #     plt.show()
                #     continue

                if not len(RecTTLs):
                    RecTTLs = [(_-TTLs[0])//6 for _ in TTLs[:6]]
                    RecN = Analysis.Normalize(Rec[TTLs[0]:TTLs[6]:6,:])
                else:
                    RecTTLs = [_//6 for _ in RecTTLs]
                    RecN = Analysis.Normalize(Rec[PlotRange,:])

                Colors = ['silver']*RecN.shape[1]
                if max([DataInfo['DAqs']['TTLCh']]+DataInfo['DAqs']['RecCh']) <= RecN.shape[1]:
                    Colors[DataInfo['DAqs']['TTLCh']-1] = 'b'
                    if 'StimCh' in DataInfo['DAqs']: Colors[DataInfo['DAqs']['StimCh']-1] = 'g'
                    Colors = [Color if C+1 not in DataInfo['DAqs']['RecCh'] else 'k' for C,Color in enumerate(Colors)]

                print('Data:', DataInfo['DAqs']['RecCh'])
                print('TTL:', DataInfo['DAqs']['TTLCh'])
                Fig, Ax = plt.subplots()

                for TTL in RecTTLs:
                    Ax.axvspan(TTL, TTL+0.003*Rate[P]//6, color='r', alpha=0.3)

                Ax = Plot.AllCh(RecN, lw=0.8, Ax=Ax,Colors=Colors,
                                AxArgs={'title':Exp.split('/')[-2]+'_'+Exp.split('/')[-1].split('_')[-1]+'_'+P+'_'+R})

                # SquareY = Ax.get_lines()[DataInfo['DAqs']['TTLCh']-1].get_ydata()
                # SquareY = [SquareY.min(), SquareY.max(), SquareY.max(), SquareY.min(), SquareY.min()]
                # SquareX = [0, 0, RecN.shape[0], RecN.shape[0], 0]
                # Ax.plot(SquareX, SquareY, color='silver', linestyle='--')

                plt.show()
                ## ====

                print(''); print('='*20); print('')

                ## Restore channels if changed
                # if ChangedCh:
                #     DataInfo['DAqs']['RecCh'], DataInfo['DAqs']['TTLCh'] = RecCh, TTLCh
                ## ====


# for TTL in TTLs: plt.axvspan(TTL, TTL+0.003*Rate[P], color='r', alpha=0.3)
# plt.plot(Rec[:,-1])
# plt.plot([0,len(Rec)], [-232]*2)
# plt.show()



#%% Find TTLs for RecControl

FilterFreq = [600,1500]
SampleStart = []

for F,Folder in enumerate([
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_01-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_02-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_04-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_05-UnitRec'
    ]):

    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')[0]
    DataInfo = IO.Txt.Read(DataInfo)

    for E, Exp in enumerate(Exps):
        if 'asal' in Exp or 'aselin' in Exp: continue
        if 'asal' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz'] or 'aselin' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']:
            continue

        print('Loading', Exp, '...')
        Data, Rate = IO.DataLoader(Exp, 'Bits')

        for P,Proc in Data.items():
            if Data[P][list(Data[P].keys())[0]].shape[1] in [1,16]: continue

            for R,Rec in Proc.items():
                # TTLs = GetTTLs(Rec, Rate[P], DataInfo, Exp, True, R, Freq+'_'+"{0:02d}".format(int(E)))
                TTLs = Analysis.GenTTLsRising(Rate[P], DataInfo['Audio']['SoundPulseDur'], DataInfo['Audio']['SoundPauseBeforePulseDur'], DataInfo['Audio']['SoundPauseAfterPulseDur'], 0, DataInfo['Audio']['SoundPulseNo'])
                ABR = ABRs.ABRPerCh(Rec, Rate[P], TTLs, DataInfo['DAqs']['RecCh'], [0, 0.1], FilterFreq, 4)

                #===
                Fig, Axes = plt.subplots(3,1,figsize=(10,10), sharex=True)

                Axes[0].plot(ABR, alpha=0.3)
                Axes[1].plot(abs(Analysis.GetInstFreq(ABR, Rate[P])), alpha=0.3)
                Axes[2].plot(Rec[:ABR.shape[0],16:])

                Axes[1].set_ylim(FilterFreq)
                Fig.suptitle('Where is the click?')
                plt.show()
                #===

                SS = input('Sample where clicks start: ')
                SampleStart.append([F,E,R,int(SS)])


#%% Update NoTTLs.dict
Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-ABRs') + glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-UnitRec'))
NoTTLsFile = os.environ['NEBULAPATH']+'/Documents/PhD/Notes/Experiments/NoTTLRecs.dict'
NoTTLRecs = IO.Txt.Read(NoTTLsFile)
ToAddHz = []
WrongExpNo = []

for F,Folder in enumerate(Folders):
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')
    if len(DataInfo) > 1:
        WrongExpNo.append(Folder)
        continue
    else:
        DataInfo = DataInfo[0]

    DataInfo = IO.Txt.Read(DataInfo)
    if 'Hz' not in DataInfo['ExpInfo']['00'].keys():
        ToAddHz.append(Folder)
        continue

    if len(Exps) != len(DataInfo['ExpInfo']):
        WrongExpNo.append(Folder)
        continue

    for E, Exp in enumerate(Exps):
        Freq = DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']
        nF = DataInfo['InfoFile'].split('/')[-1].split('.')[0]

        if nF in NoTTLRecs:
            if Freq in NoTTLRecs[nF]:
                print('Exp', E, 'Rec', R, 'Freq', Freq)
                NoTTLRecs[nF][Freq+'_'+"{0:02d}".format(int(E))] = NoTTLRecs[nF].pop(Freq)

# IO.Txt.Write(NoTTLRecs, NoTTLsFile)
