#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@date: 2019
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import numpy as np
from glob import glob
from os import sep

import sciscripts.Intan_RHA


## Level 0
def Load(File, ChannelMap=[]):
    Data = Intan_RHA.ReadData(File)

    try: len(Data['aux'][0])
    except TypeError: Data['aux'] = Data['aux'].reshape((Data['aux'].shape[0], 1))

    Data = np.hstack((Data['analog'], Data['aux']))

    if ChannelMap:
        ChannelMap = [_-1 for _ in ChannelMap]
        Data[:, (range(len(ChannelMap)))] = Data[:, ChannelMap]

    Rate = 25000

    return(Data, Rate)


def RemapCh(Probe, Adaptor):
    """ Returns the channel order according to the connector"""

    Probes = {
        'CM16': {'Tip': [9, 8, 10, 7, 11, 6, 12, 15, 13, 4, 14, 3, 15, 2, 16, 1],
                 'Head': [12, 11, 10, 9, 8, 7, 6, 5, 16, 15, 14, 13, 4, 3, 2, 1]},
    }

    Adaptors = {
        'RHAOM': [12, 11, 10, 9, 8, 7, 6, 5, 13, 14, 15, 16, 1, 2, 3, 4],
    }

    Tip = Probes[Probe]['Tip']
    Head = Probes[Probe]['Head']
    Connector = Adaptors[Adaptor]

    print('Get channel order... ', end='')
    ChNo = len(Tip)
    ChMap = [0]*ChNo

    for Ch in range(ChNo):
        TipCh = Tip[Ch] # What channel should be the Ch
        HeadCh = Head.index(TipCh) # Where Ch is in Head
        ChMap[Ch] = Connector[HeadCh] # Channels in depth order

    print('Done.')
    return(ChMap)


# Level 1
def FolderLoad(Folder, ChannelMap=[]):
    Rate = 25000
    Data = {str(F): Load(File, ChannelMap)[0]
            for F,File in enumerate(sorted(glob(Folder+sep+'*int')))}

    return(Data, Rate)


# Level 2
def DataLoader(Folder, ChannelMap=[]):
    FilesExt = [F.split('.')[-1] for F in glob(Folder+sep+'*.*')]

    if np.unique(FilesExt).tolist() == ['int']:
        Data, Rate = FolderLoad(Folder, ChannelMap)
    elif Folder[-4:] == '.int':
        Data, Rate = Load(Folder, ChannelMap)
    else:
        print('Data format not supported.'); return(None)

    return(Data, Rate)


# Level 3
def ERPsLoad(Folder):
    """ Loads intan files from folder """

    ChMap = RemapCh('CM16', 'RHAOM') + [17]
    Data, Rate = DataLoader(Folder, ChannelMap=ChMap)

    D = np.vstack([Data[_] for _ in sorted(Data.keys(), key=lambda x: int(x))])
    Data = {'100': {'0': D}}
    Rate = {'100': Rate}

    Key = list(Data.keys())[0]
    Rec = sorted(Data[Key].keys(), key=lambda x: [int(i) for i in x])[0]
    Data, Rate = Data[Key][Rec], Rate[Key]

    return(Data, Rate)


#%% Dataset info
ERPsFolder = 'FolderWhereDataFromDriveIsSaved' # Change to the right location
TreatmentOrder = ['NaCl', 'Ket5mg', 'Ket20mg']

Folders = sorted(glob(sep.join([ERPsFolder,'2*ERPs','20??-*'])))
Folders = [_ for _ in Folders if _.split('_')[-1] in TreatmentOrder and '2013-07-23_11-11-58' not in _]
Treatments = np.array([_.split('_')[-1] for _ in Folders])

Channels = np.array([
    9, 4, 4,
    7, 10, 10,
    9, 4, 4,
    10, 7, 7,
    7, 6, 6,
    4, 4, 4,
    6, 6, 6,
    1, 1, 1,
    5, 5,
    1, 1, 1,
    1, 1, 1,
    16, 16, 16,
    4, 4, 4,
    7, 7, 7,
    7, 7, 7,
    2,
    1, 1, 1,
    4, 4, 4
])

""" Example of how to load the data:

for F,Folder in enumerate(Folders):
    Data, Rate = ERPsLoad(Folder)
    Treatment = Treatments[F]
    ChosenChannel = Channels[F]

    # Then run the desired analysis
"""
