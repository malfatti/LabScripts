# LabScripts  
Scripts for controlling devices/running experiments/analyzing data.


## Dependencies
- Linux, Bash
- neo, numpy, quantities, scipy, sciscripts


## Installation

Below are instructions to make everything work. This assumes that data is on `~/Data`, analysis is on `~/Analysis` and that both this and the Open-ephys `analysis-tools` repositories are on `~/Git`. It also assumes that `R`, `Portaudio` and `HDF5` are already installed.

```bash
$ pip install --user neo quantities sciscripts

$ mkdir ~/{Data,Analysis,Git}

$ cd ~/Git

$ git clone https://gitlab.com/malfatti/LabScripts

$ git clone https://github.com/open-ephys/analysis-tools

$ cd ~/

$ echo 'export DATAPATH=~/Data \nexport ANALYSISPATH=~/Analysis' >> ~/.profile

$ for f in ~/.local/lib*/python3*/site-packages/; do echo "$HOME"/Git/analysis-tools/Python3/ > "$f"/Path.pth; done

```

