/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

int analogPin = 2;
int val = 0;

void setup()
{
  Serial.begin(38400);
  analogReference(INTERNAL);
}

void loop()
{
  val = analogRead(analogPin);
//  val = map(val, 0, 1023, 0, 255);

  Serial.println(val);
}
