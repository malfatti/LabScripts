/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Arduino will read an analog in and, depending on the voltage, it will turn on a
digital port, or both. This provides 5V TTLs even if you have analog small voltage.
*/
const int SoundAndLaserTTLIn = 1;
const int LaserOut =  2;
const int SoundTTLOut = 4;

void setup() {
  Serial.begin(38400);
  analogReference(INTERNAL);

  pinMode(SoundAndLaserTTLIn, INPUT);
  pinMode(LaserOut, OUTPUT);
  pinMode(SoundTTLOut, OUTPUT);

  digitalWrite(LaserOut, LOW);
  digitalWrite(SoundTTLOut, LOW);
}

void loop() {

  int SoundAndLaserTTLInV = analogRead(SoundAndLaserTTLIn);
  SoundAndLaserTTLInV = map(SoundAndLaserTTLInV, 0, 1023, 0, 255);

  if (SoundAndLaserTTLInV < 30) {
    digitalWrite(LaserOut, LOW);
    digitalWrite(SoundTTLOut, LOW);
  }

  if (SoundAndLaserTTLInV >= 55 && SoundAndLaserTTLInV < 85) {
    digitalWrite(LaserOut, HIGH);
    digitalWrite(SoundTTLOut, LOW);
  }

  if (SoundAndLaserTTLInV > 160 && SoundAndLaserTTLInV < 185) {
    digitalWrite(LaserOut, LOW);
    digitalWrite(SoundTTLOut, HIGH);
  }

  if (SoundAndLaserTTLInV >= 240) {
    digitalWrite(LaserOut, HIGH);
    digitalWrite(SoundTTLOut, HIGH);
  }


}
