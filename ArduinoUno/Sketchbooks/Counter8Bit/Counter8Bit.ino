/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

LedNo = 8
Pins[8] = {6, 7, 8, 9, 10, 11, 12, 13};

void setup() {
  for(int Pin = 0; Pin < LedNo; Pin++) {
    digitalWrite(Pin, LOW)
  }

}

void loop() {
  for(int i = 0, x = 1; i < binLength; i++, x+=2) { 
      if(binNumber[i] == '0') state = LOW;
      if(binNumber[i] == '1') state = HIGH;
      digitalWrite(pins[i] + binLength - x, state);
    } 
}
