/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

  A = pulse on pin 2
  B = pulse on pin 4
  C = pulse on pin 7
  D = pulse on pin 8
  E = pulse on pin 10
  F = pulse on pin 11
  G = pulse on pin 12
  P = pulse on pin 13
*/

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

const int inSoundPin = 2;
const int outSoundPin = 2;
const int outLaserPin = 4;

void setup() {
  // set prescale to 16
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;

  Serial.begin(38400);
  analogReference(INTERNAL);

  pinMode(inSoundPin, INPUT);
  pinMode(outSoundPin, OUTPUT);
  pinMode(outLaserPin, OUTPUT);
  digitalWrite(outSoundPin, LOW);
  digitalWrite(outLaserPin, LOW);

  int inPinV = 0;
}

void loop() {
  cli();
  while (1) {
    int inPinV = analogRead(inSoundPin);

    if (inPinV >= 0 && inPinV < 30) {
      PORTD &= ~_BV (outSoundPin);
      PORTD &= ~_BV (outLaserPin);
      //    bitClear (PORTD, Pins[0]);
      //    bitClear (PORTD, Pins[1]);
      //    PORTD = B00000000;
      //    digitalWrite(Pins[0], LOW);
      //    digitalWrite(Pins[1], LOW);
    }

    if (inPinV >= 210 && inPinV < 180) {
      PORTD &= ~_BV (outSoundPin);
      PORTD |= _BV (outLaserPin);
      //    bitClear (PORTD, Pins[0]);
      //    bitSet (PORTD, Pins[1]);
      //    PORTD = B00010000;
      //    digitalWrite(Pins[0], LOW);
      //    digitalWrite(Pins[1], HIGH);
    }

    if (inPinV >= 420 && inPinV < 440) {
      //      PORTD |= _BV (outSoundPin);
      //      PORTD &= ~_BV (outLaserPin);
      //    bitSet (PORTD, Pins[0]);
      //    bitClear (PORTD, Pins[1]);
      PORTD = B00000100;
      //    digitalWrite(Pins[0], HIGH);
      //    digitalWrite(Pins[1], LOW);
    }

    if (inPinV >= 600) {
      PORTD |= _BV (outSoundPin);
      PORTD |= _BV (outLaserPin);
      //    bitSet (PORTD, Pins[0]);
      //    bitSet (PORTD, Pins[1]);
      //    PORTD = B00010100;
      //    digitalWrite(Pins[0], HIGH);
      //    digitalWrite(Pins[1], HIGH);
    }
  }
}
