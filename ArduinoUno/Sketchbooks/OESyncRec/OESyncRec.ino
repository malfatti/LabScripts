/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2021-08-15
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/LabScripts

Serial-controlled TTL generator. Useful for triggering recordings and
synchronize recordings with other devices.

    p:
        1) Turn Pins[7] HIGH;
        2) Pulse on Pins[6] with duration set in line 24 as `Delay`;
        3) Keep blinking Pins[5] with HIGH and LOW time set in line 24 as `Delay`.

    s (only work after `p` was sent):
        1) Stop blinking Pins[5];
        2) Pulse on Pins[6] with duration set in line 30 as `Delay`;
        3) Turn Pins[7] LOW.

*/

const int PinNo = 8;
const int Pins[PinNo] = {2, 4, 7, 8, 10, 11, 12, 13};
const int Delay = 50;

void setup() {
    Serial.begin(115200);

    for (int Pin = 0; Pin < PinNo; Pin++) {
        pinMode(Pins[Pin], OUTPUT);
        digitalWrite(Pins[Pin], LOW);
    }

    char ch = 0;
    int inPinV = 0;
}

void loop() {
    char ch = 0;
    int inPinV = 0;

    while (ch == 0) { ch = Serial.read(); }

    if (ch == 'p') {
        digitalWrite(Pins[7], HIGH);
        digitalWrite(Pins[6], HIGH);
        delay(Delay);
        digitalWrite(Pins[6], LOW);

        while (ch != 's') {
            digitalWrite(Pins[5], HIGH);
            delay(Delay);
            digitalWrite(Pins[5], LOW);
            delay(Delay);
            ch = Serial.read();
        }
    
        digitalWrite(Pins[6], HIGH);
        delay(Delay);
        digitalWrite(Pins[6], LOW);
        digitalWrite(Pins[7], LOW);
    }
}

