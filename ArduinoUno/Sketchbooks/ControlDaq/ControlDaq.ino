/* 
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

In this program, Arduino will read serial input and, depending on the value, 
it will turn on and off (5ms delay) a digital pin. We use this to sinchronize 
DAQs in different computers.

Sumarizing the commands:
A = turn D13 on
*/

const int Pin13 =  13;

void setup() {
  Serial.begin(19200);
  pinMode(Pin13, OUTPUT);

  digitalWrite(Pin13, LOW);

  char ch = 0;
}

void loop() {

  char ch;

  while (ch == 0) {
    ch = Serial.read();
  }

  if (ch == 'A') {
      digitalWrite(Pin13, HIGH);
      delay(2)
      digitalWrite(Pin13, LOW);
  }

}
