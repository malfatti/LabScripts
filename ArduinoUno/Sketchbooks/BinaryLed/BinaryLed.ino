/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

int NI = 7;
int Led = 8;

void setup() {
  pinMode(NI, INPUT);
  pinMode(Led, OUTPUT);
  digitalWrite(Led, LOW);
  
}

void loop() {
  if(digitalRead(NI) == HIGH) {
    digitalWrite(Led, HIGH);
  }
  else {
    digitalWrite(Led, LOW);
  }
}
