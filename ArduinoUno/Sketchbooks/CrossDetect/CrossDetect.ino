/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

const int InPin = A0;
const int OutPin = 13;
const int Thr = 10;
const int PulseNo = 4;
const int PulseDur = 3;
const int PulsePause = 63;
const int BlockNo = 6;
const int BlockPause = 500-PulseDur-PulsePause;
const int SafePause = 0;

void setup() {
    pinMode(OutPin, OUTPUT); digitalWrite(OutPin, LOW);

    // Set free running mode on ADC7 (pin A0)
    ADC->ADC_MR |= 0x80;
    ADC->ADC_CR = 2;
    ADC->ADC_CHER = 0x80;
    pinMode(InPin, INPUT);

}

void loop() {
    int inPinV = 0;

    while ((ADC->ADC_ISR & 0x80) == 0); // wait for conversion
    inPinV = ADC->ADC_CDR[7];
    
    if (inPinV < Thr) {
        for (int Block = 0; Block < BlockNo; Block++) {
            for (int Pulse = 0; Pulse < PulseNo; Pulse++) {
                digitalWrite(OutPin, HIGH); delay(PulseDur);
      	        digitalWrite(OutPin, LOW); delay(PulsePause);
            }

            delay(BlockPause);
        }

        delay(SafePause);
    }
}

