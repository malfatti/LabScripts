/*
  @author: T. Malfatti <malfatti@disroot.org>
  @date: 2018-06-06
  @license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
  @homepage: https://gitlab.com/Malfatti/SciScripts

  Arduino will read all analog inputs and print them in serial,
  tab separated, plus the cpu time in milliseconds.
*/

const int InAPinNo = 6;
const int InAPins[InAPinNo] = {A0, A1, A2, A3, A4, A5};
const int InDPinNo = 8;
const int InDPins[InDPinNo] = {2, 3, 4, 5, 6, 7, 8, 9};

void setup() {
  Serial.begin(115200);
  analogReference(INTERNAL);

  for (int Pin = 0; Pin < InAPinNo; Pin++) {
    pinMode(InAPins[Pin], INPUT);
  }

  for (int Pin = 0; Pin < InDPinNo; Pin++) {
    pinMode(InDPins[Pin], INPUT);
  }

  pinMode(12, OUTPUT); digitalWrite(12, LOW);
  pinMode(13, OUTPUT); pinMode(13, LOW);
}


void loop() {
  Serial.print(millis());
  Serial.print(",");
  for (int Pin = 0; Pin < InDPinNo; Pin++) {
    Serial.print(digitalRead(InDPins[Pin]));
    Serial.print(",");
  }
  for (int Pin = 0; Pin < InAPinNo; Pin++) {
    Serial.print(analogRead(InAPins[Pin]));
    Serial.print(",");
  }
  Serial.print(millis());
  Serial.print("\r\n");
}
