/*
  @author: T. Malfatti <malfatti@disroot.org>
  @date: 2018-06-06
  @license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
  @homepage: https://gitlab.com/Malfatti/SciScripts

  Arduino will read  analog inputs and print them in serial,
  tab separated, plus the cpu time in milliseconds.
*/

const int InPinNo = 6;
const int InPins[InPinNo] = {A0, A1, A2, A3, A4, A5};

void setup() {
  Serial.begin(115200);
  analogReference(INTERNAL);

  DDRB =  0b00000000;
  PORTB = 0b00000000;

  for (int Pin = 0; Pin < InPinNo; Pin++) {
    pinMode(InPins[Pin], INPUT);
  }

}


void loop() {
  Serial.print("D[");
  Serial.print(PINB);
  Serial.print(",");
  Serial.print(millis());
  Serial.print("]");
  for (int Pin = 0; Pin < InPinNo; Pin++) {
    Serial.print("A");
    Serial.print(Pin);
    Serial.print("[");
    Serial.print(analogRead(InPins[Pin]));
    Serial.print(",");
    Serial.print(millis());
    Serial.print("]");
  }
  Serial.print("\r\n");
}
