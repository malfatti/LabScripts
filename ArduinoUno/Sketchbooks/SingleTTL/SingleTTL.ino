/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2020-02-05
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

const int TTL = 13;

void setup() {
  pinMode(TTL, OUTPUT);
  digitalWrite(TTL, LOW);

  digitalWrite(TTL, HIGH);
  delay(5000);
  digitalWrite(TTL, LOW);
}

void loop() {
}
